import uuid

import redis
from redis.commands.search.field import TextField, NumericField
from redis.commands.search.indexDefinition import IndexDefinition, IndexType

import schema.models


class Db:
    instance = None

    class Meta:
        db_name = "pizza_redis"
        prefix_template = "{db_name}:{key_type}"
        key_template = "{prefix}:{key}:"

    @classmethod
    def get_instance(cls):
        if not cls.instance:
            cls.instance = redis.Redis(decode_responses=True)
        return cls.instance

    @classmethod
    def get_key(cls, key_type: str, key: str = None):
        return cls.Meta.key_template.format(
            prefix=cls.get_prefix(key_type), key=key or uuid.uuid4()
        )

    @classmethod
    def get_prefix(cls, key_type: str):
        return cls.Meta.prefix_template.format(
            db_name=cls.Meta.db_name, key_type=key_type, key="{}"
        )


INGREDIENT_SCHEMA = (TextField("name"), NumericField("type"))
ORDER_SCHEMA = (NumericField("is_ready"),)


def create_indexes():
    db = Db.get_instance()

    try:
        db.ft(f"{Db.get_prefix(schema.models.Ingredient.__name__)}:idx").dropindex()
    except redis.exceptions.ResponseError:
        pass
    db.ft(f"{Db.get_prefix(schema.models.Ingredient.__name__)}:idx").create_index(
        INGREDIENT_SCHEMA,
        definition=IndexDefinition(
            prefix=[Db.get_prefix(schema.models.Ingredient.__name__)], index_type=IndexType.HASH
        ),
    )


def create_data():
    initial_data = [
        schema.models.Ingredient(
            name="pate",
            weight=20,
            price=0.2,
            cook_time=30,
            type=schema.models.IngredientType.BASE
        ),

        schema.models.Ingredient(
            name="peperoni",
            weight=5,
            price=0.6,
            cook_time=15,
            type=schema.models.IngredientType.TOPPING
        ),
        schema.models.Ingredient(
            name="mozzarella",
            weight=20,
            price=0.2,
            cook_time=30,
            type=schema.models.IngredientType.TOPPING
        ),
        schema.models.Ingredient(
            name="tomates",
            weight=10,
            price=0.3,
            cook_time=20,
            type=schema.models.IngredientType.TOPPING
        ),

        schema.models.Ingredient(
            name="sauce tomate",
            weight=10,
            price=0.1,
            cook_time=5,
            type=schema.models.IngredientType.SAUCE
        ),
    ]

    db = Db.get_instance()

    [db.delete(key) for key in db.scan(count=1000, match=f"{Db.Meta.db_name}*")[1]]

    for ingredient in initial_data:
        key, ingredient = ingredient.to_redis()
        db.hset(key, mapping=ingredient)


if __name__ == '__main__':
    print("create initial data")
    create_data()
    print("creating indexes")
    create_indexes()
