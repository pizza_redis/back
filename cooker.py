import time

import db.database
from schema.models import Ingredient

if __name__ == '__main__':
    redis = db.database.Db.get_instance()
    p = redis.pubsub()
    p.psubscribe('__keyevent@0__:expired')
    while True:
        msg = p.get_message()
        if msg and msg['pattern']:
            print(msg)
            key = msg['data'][0:-4]
            redis.hset(key, 'is_ready', 1)
        time.sleep(0.5)
