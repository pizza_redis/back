from strawberry.asgi import GraphQL

from schema.schema import schema

app = GraphQL(schema)
