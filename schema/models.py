import uuid
from enum import Enum

import strawberry
from redis import Redis

import db.database


@strawberry.enum
class IngredientType(Enum):
    BASE = 0
    TOPPING = 1
    SAUCE = 2


@strawberry.type
class Ingredient:
    id: uuid.UUID = strawberry.field(default_factory=uuid.uuid4)
    name: str
    weight: int
    price: float
    cook_time: int
    type: IngredientType

    def to_redis(self):
        return (
            db.database.Db.get_key(self.__class__.__name__, str(self.id)),
            {
                "name": self.name,
                "weight": self.weight,
                "price": self.price,
                "cook_time": self.cook_time,
                "type": self.type.value,
            }
        )

    @classmethod
    def from_redis(cls, raw_ingredient: dict):
        return Ingredient(
            id=raw_ingredient["id"],
            name=raw_ingredient["name"],
            weight=int(raw_ingredient["weight"]),
            price=float(raw_ingredient["price"]),
            cook_time=int(raw_ingredient["cook_time"]),
            type=IngredientType(int(raw_ingredient["type"])),
        )


@strawberry.type
class Order:
    id: uuid.UUID = strawberry.field(default_factory=uuid.uuid4)
    ingredients: list[Ingredient]
    # owner_id: uuid.UUID = strawberry.field(default=uuid.uuid4)
    is_ready: bool = strawberry.field(default=False)

    @strawberry.field(name="price")
    def price_field(self) -> float:
        return self.price

    @strawberry.field(name="cookTime")
    def cook_time_field(self) -> int:
        return self.cook_time

    @strawberry.field(name="weight")
    def weight_field(self) -> int:
        return self.weight

    @property
    def price(self):
        return sum([ingredient.price for ingredient in self.ingredients])

    @property
    def cook_time(self) -> int:
        return sum([ingredient.cook_time for ingredient in self.ingredients])

    @property
    def weight(self) -> int:
        return sum([ingredient.weight for ingredient in self.ingredients])

    def to_redis(self):
        return (
            db.database.Db.get_key(self.__class__.__name__, str(self.id)),
            {
                "is_ready": int(self.is_ready),
                "cook_time": self.cook_time,
                "weight": self.weight,
                "price": self.price,
            },
            [db.database.Db.get_key(Ingredient.__name__, str(ingredient.id)) for ingredient in self.ingredients]
        )

    @classmethod
    def from_redis(cls, raw_order: dict):
        redis = db.database.Db.get_instance()

        ingredients = []
        for key in raw_order["ingredients"]:
            raw_ingredient: dict = redis.hgetall(key)
            raw_ingredient['id'] = key.split(":")[-1]
            ingredients.append(Ingredient.from_redis(raw_ingredient))
        return Order(
            id=raw_order["id"],
            is_ready=bool(raw_order["is_ready"] == '1'),
            ingredients=ingredients
        )


@strawberry.input
class OrderInput:
    base: uuid.UUID
    toppings: list[uuid.UUID]
    sauce: uuid.UUID
