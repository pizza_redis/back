import asyncio
from typing import AsyncGenerator

import strawberry

import db.database
from schema.models import Ingredient, Order, OrderInput, IngredientType


@strawberry.type
class Query:
    @strawberry.field
    def ingredients(self) -> list[Ingredient]:
        ingredients = []

        redis = db.database.Db.get_instance()
        _, keys = redis.scan(match=f"{db.database.Db.get_prefix(Ingredient.__name__)}*:", count=1000)

        for key in keys:
            raw_ingredient: dict = redis.hgetall(key)
            raw_ingredient['id'] = key.split(":")[-2]
            ingredients.append(Ingredient.from_redis(raw_ingredient))

        return ingredients

    @strawberry.field
    def orders(self) -> list[Order]:
        orders = []

        redis = db.database.Db.get_instance()
        _, keys = redis.scan(match=f"{db.database.Db.get_prefix(Order.__name__)}*:", count=1000)

        for key in keys:
            raw_order: dict = redis.hgetall(key)
            raw_order['id'] = key.split(":")[-2]

            ingredients = []

            ingredients_keys = redis.lrange(f"{key}ingredients", start=0, end=100)

            raw_order['ingredients'] = ingredients_keys

            orders.append(Order.from_redis(raw_order))

        return orders

    order: Order


@strawberry.type
class Mutation:
    @strawberry.mutation
    def place_order(self, order: OrderInput) -> Order:
        redis = db.database.Db.get_instance()

        if not redis.hgetall(db.database.Db.get_key(Ingredient.__name__, str(order.base)))['type'] == str(
                IngredientType.BASE.value):
            raise TypeError("invalid base type")
        if not redis.hgetall(db.database.Db.get_key(Ingredient.__name__, str(order.sauce)))['type'] == str(
                IngredientType.SAUCE.value):
            raise TypeError("invalid sauce type")

        for topping in order.toppings:
            if not redis.hgetall(db.database.Db.get_key(Ingredient.__name__, str(topping)))['type'] == str(
                    IngredientType.TOPPING.value):
                raise TypeError(f"invalid topping type for topping {topping}")

        ingredients = []
        for ingredient_id in [order.base, *order.toppings, order.sauce]:
            ingredient = redis.hgetall(db.database.Db.get_key(Ingredient.__name__, str(ingredient_id)))
            ingredients.append(Ingredient.from_redis({
                **ingredient,
                "id": ingredient_id
            }))

        order = Order(
            ingredients=ingredients
        )

        redis.hset(order.to_redis()[0], mapping=order.to_redis()[1])
        redis.lpush(f"{order.to_redis()[0]}ingredients", *order.to_redis()[2])
        redis.set(f"{order.to_redis()[0]}cook", 0, ex=order.cook_time)

        return order


@strawberry.type
class Subscription:
    @strawberry.subscription
    async def cooked(self, target: str) -> AsyncGenerator[bool, None]:
        p = db.database.Db.get_instance().pubsub()
        p.psubscribe(f'__keyspace@0__:{db.database.Db.get_key(Order.__name__, target)}cook')
        while True:
            msg = p.get_message()
            if msg and msg['pattern']:
                print(msg)
                if msg['data'] == "expired":
                    yield True
                    break
            await asyncio.sleep(0.5)


schema = strawberry.Schema(query=Query, mutation=Mutation, subscription=Subscription)
